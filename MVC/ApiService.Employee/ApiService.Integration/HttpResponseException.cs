﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace ApiService.Integration
{
    [Serializable]
    internal class HttpResponseException : Exception
    {
        private HttpStatusCode statusCode;

        public HttpResponseException()
        {
        }

        public HttpResponseException(HttpStatusCode statusCode)
        {
            this.statusCode = statusCode;
        }

        public HttpResponseException(string message) : base(message)
        {
        }

        public HttpResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HttpResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}