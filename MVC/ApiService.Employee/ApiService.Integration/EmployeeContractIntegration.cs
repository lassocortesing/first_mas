﻿using ApiService.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Integration
{
    public class EmployeeContractIntegration
    {
        public EmployeeContractIntegration()
        {

        }
        public List<RawEmployeeContract> GetAllEmployee()
        {
            try
            {
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                List<RawEmployeeContract> apiRawResult = new List<RawEmployeeContract>();
                string json = client.DownloadString(string.Concat(RequestConstants.BaseUrl, RequestConstants.ApiRequestEmployee));
                apiRawResult = JsonConvert.DeserializeObject<List<RawEmployeeContract>>(json);
                return apiRawResult;
            }
            catch (Exception errorComunication)
            {
                throw new HttpResponseException(errorComunication.Message, errorComunication);
            }
        }
    }
}

