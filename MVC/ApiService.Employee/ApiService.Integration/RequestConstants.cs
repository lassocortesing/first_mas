﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Integration
{
    public static class RequestConstants
    {
        public const string BaseUrl = "http://masglobaltestapi.azurewebsites.net";
        public const string ApiRequestEmployee = "/api/Employees";
    }
}
