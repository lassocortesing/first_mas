﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiService.Domain
{
    public class ReportEmployeeContract
    {
        [JsonProperty(PropertyName = "Id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }
        
        [JsonProperty(PropertyName = "ContractTypeName")]
        public string ContractTypeName { get; set; }

        [JsonProperty(PropertyName = "RoleName")]
        public string RoleName { get; set; }
        
        [JsonProperty(PropertyName = "RoleDescription")]
        public string RoleDescription { get; set; }

        [JsonProperty(PropertyName = "HourlySalary")]
        public decimal HourlySalary { get; set; }

        [JsonProperty(PropertyName = "MonthlySalary")]
        public decimal MonthlySalary { get; set; }

        [JsonProperty(PropertyName = "AnnualSalary")]
        public decimal AnnualSalary { get; set; }

    }
}
