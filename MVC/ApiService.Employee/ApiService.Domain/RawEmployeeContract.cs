﻿using Newtonsoft.Json;
using System;

namespace ApiService.Domain
{
    public class RawEmployeeContract
    {
        [JsonProperty(PropertyName = "Id")]
        public  int Id { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ContractTypeName")]
        public string ContractTypeName { get; set; }

        [JsonProperty(PropertyName = "RoleId")]
        public int RoleId { get; set; }

        [JsonProperty(PropertyName = "RoleName")]
        public string RoleName { get; set; }

        [JsonProperty(PropertyName = "RoleDescription")]
        public string RoleDescription { get; set; }

        [JsonProperty(PropertyName = "HourlySalary")]
        public decimal HourlySalary { get; set; }

        [JsonProperty(PropertyName = "MonthlySalary")]
        public decimal MonthlySalary { get; set; }

        public decimal GetSalary()
        {
            switch (ContractTypeName)
            {
                case "HourlySalaryEmployee":
                    return HourlySalary;

                case "MonthlySalaryEmployee":
                    return MonthlySalary;

                default:
                    return 0;

            }
        }

    }
}
