﻿using ApiService.Services;
using System.Threading.Tasks;
using System.Web.Http;

namespace ApiService.Employee.Controllers
{
    [Route("api/EmployeeContractData")]

    public class EmployeeContractDataController : ApiController
    {
        [HttpGet]
        [Route("api/EmployeeContractData/GetAll")]
        public object GetAll()
        {
            EmployeeContractServices start = new EmployeeContractServices();
            return Ok(start.GetAllEmployeeServices());
        }

        [HttpGet]
        [Route("api/EmployeeContractData/GetId/{id}")]
        public object GetId(int id)
        {
            EmployeeContractServices start = new EmployeeContractServices();
            return Ok(start.GetEmployeeId(id));
        }



    }
}