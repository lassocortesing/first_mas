﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    class HourlyContract : IContractType
    {
        private readonly decimal _hourlySalary;
        private decimal _anualSalary;

        public HourlyContract(decimal hourlySalary)
        {
            _hourlySalary = hourlySalary;
        }
        public decimal Calculate()
        {
            _anualSalary = _hourlySalary * 120 * 12;
            return _anualSalary;
        }
    }
}
