﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    class HourlySalaryEmployeeFactory : ContractTypeFactory
    {
        public override IContractType Create(decimal salary) => new HourlyContract(salary);

    }
}
