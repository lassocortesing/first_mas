﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod

{
    class MonthlySalaryEmployeeFactory : ContractTypeFactory
    {
        public override IContractType Create(decimal salary) => new MonthtlyContract(salary);
    }
}
