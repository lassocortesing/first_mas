﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    public enum Actions
    {
        HourlySalaryEmployee,
        MonthlySalaryEmployee
    }
}
