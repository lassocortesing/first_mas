﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    class EmployeeContract
    {
        private readonly Dictionary<Actions, ContractTypeFactory> _factories;

        private EmployeeContract()
        {
            _factories = new Dictionary<Actions, ContractTypeFactory>();

            foreach (Actions iAction in Enum.GetValues(typeof(Actions)))
            {
                string nameFactoryClass = "ApiService.Services.FactoryMethod.";
                string suffixFactory = "Factory";
                var factory = (ContractTypeFactory)Activator.CreateInstance(Type.GetType(string.Concat(nameFactoryClass, Enum.GetName(typeof(Actions), iAction), suffixFactory)));
                _factories.Add(iAction, factory);
            }
        }

        public static EmployeeContract InitializeFactory() => new EmployeeContract();

        public IContractType ExecuteCreation(Actions action, decimal salary) => _factories[action].Create(salary);
    }
}
