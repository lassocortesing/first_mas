﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    class MonthtlyContract : IContractType
    {
        private readonly decimal _monthSalary;
        private decimal _anualSalary;

        public MonthtlyContract(decimal monthtlySalary)
        {
            _monthSalary = monthtlySalary;
        }
        public decimal Calculate()
        {
            _anualSalary = _monthSalary * 12;
            return _anualSalary;
        }
    }
}
