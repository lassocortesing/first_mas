﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Services.FactoryMethod
{
    public abstract class ContractTypeFactory
    {
        public abstract IContractType Create(decimal salary);
    }
}
