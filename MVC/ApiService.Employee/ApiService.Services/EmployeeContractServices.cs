﻿using ApiService.Domain;
using ApiService.Integration;
using ApiService.Services.FactoryMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiService.Services
{
    public class EmployeeContractServices
    {

        private List<RawEmployeeContract> LstApiExternalResult { get; set; }
        private List<ReportEmployeeContract> LstApiResult { get; set; }
        public EmployeeContractServices() { }

        public object GetAllEmployeeServices()
        {
            try
            {
                EmployeeContractIntegration apiConsumeRawObject = new EmployeeContractIntegration();
                LstApiExternalResult = apiConsumeRawObject.GetAllEmployee();
                Actions rawResult = new Actions();
                LstApiResult = (from r in LstApiExternalResult
                                select new ReportEmployeeContract
                                {
                                    Id = r.Id,
                                    Name = r.Name ?? "NA",
                                    ContractTypeName = r.ContractTypeName,
                                    RoleName = r.RoleName ?? "NA",
                                    RoleDescription = r.RoleDescription ?? "NA",
                                    HourlySalary = r.HourlySalary,
                                    MonthlySalary = r.MonthlySalary,
                                    AnnualSalary = Enum.TryParse(r.ContractTypeName, out rawResult) ? EmployeeContract.InitializeFactory().ExecuteCreation(rawResult, r.GetSalary()).Calculate() : 0
                                }).ToList();
                object objResultOk = new object[] { HttpStatusCode.Accepted, LstApiResult };
                return objResultOk;
            }
            catch (Exception generalError)
            {
                object objResultFail = new object[] { HttpStatusCode.ExpectationFailed, generalError };
                return objResultFail;
            }

        }
        public object GetEmployeeId(int parameterId)
        {
            try
            {
                EmployeeContractIntegration apiConsumeRawObject = new EmployeeContractIntegration();
                LstApiExternalResult = apiConsumeRawObject.GetAllEmployee();
                Actions rawResult;
                LstApiResult = (from r in LstApiExternalResult
                                select new ReportEmployeeContract
                                {
                                    Id = r.Id,
                                    Name = r.Name ?? "NA",
                                    ContractTypeName = r.ContractTypeName,
                                    RoleName = r.RoleName ?? "NA",
                                    RoleDescription = r.RoleDescription ?? "NA",
                                    HourlySalary = r.HourlySalary,
                                    MonthlySalary = r.MonthlySalary,
                                    AnnualSalary = Enum.TryParse(r.ContractTypeName, out rawResult) ? EmployeeContract.InitializeFactory().ExecuteCreation(rawResult, r.GetSalary()).Calculate() : 0
                                }).ToList().Where(x => x.Id == parameterId).ToList();

                object objResultOk = new object[] { HttpStatusCode.Accepted, LstApiResult };
                return objResultOk;
            }
            catch (Exception generalError)
            {
                object objResultFail = new object[] { HttpStatusCode.ExpectationFailed, generalError };
                return objResultFail;
            }
        }
    }
}
