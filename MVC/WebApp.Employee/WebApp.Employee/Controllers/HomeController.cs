﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApp.Employee.Models;

namespace WebApp.Employee.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = ".Net Developer Hands On Test";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Ing. Pedro Alexander Lasso Cortes";

            return View();
        }

        public JsonResult RequestInformation(int idEmployee)
        {
            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            client.Encoding = Encoding.UTF8;
            List<ReportEmployeeContract> apiRawResult = new List<ReportEmployeeContract>();
            string json = client.DownloadString(string.Concat(RequestConstants.BaseUrl, RequestConstants.ApiRequestById, @"/", idEmployee));
            object[] response = JsonConvert.DeserializeObject<object[]>(json);
            if (response[0].ToString().Equals("202"))
            {
                apiRawResult = JsonConvert.DeserializeObject<List<ReportEmployeeContract>>(response[1].ToString());
                object objResultOk = new object[] { true, apiRawResult };
                return Json(objResultOk, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Exception error = JsonConvert.DeserializeObject<Exception>(response[1].ToString());
                object objResultIssue = new object[] { false, error.Message };
                return Json(objResultIssue, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult RequestAllInformation()
        {

            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/json";
            client.Encoding = Encoding.UTF8;
            List<ReportEmployeeContract> apiRawResult = new List<ReportEmployeeContract>();
            string json = client.DownloadString(string.Concat(RequestConstants.BaseUrl, RequestConstants.ApiRequestAll));
            object[] response = JsonConvert.DeserializeObject<object[]>(json);
            if (response[0].ToString().Equals("202"))
            {
                apiRawResult = JsonConvert.DeserializeObject<List<ReportEmployeeContract>>(response[1].ToString());
                object objResultOk = new object[] { true, apiRawResult };
                return Json(objResultOk, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Exception error = JsonConvert.DeserializeObject<Exception>(response[1].ToString());
                object objResultIssue = new object[] { false, error.Message };
                return Json(objResultIssue, JsonRequestBehavior.AllowGet);
            }
        }
    }

}
