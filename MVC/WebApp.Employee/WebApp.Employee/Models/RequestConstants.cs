﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApp.Employee.Models
{
    public static class RequestConstants
    {
        public const string BaseUrl = "http://localhost:49634/api/";
        public const string ApiRequestById = "EmployeeContractData/GetId";
        public const string ApiRequestAll = "EmployeeContractData/GetAll";
    }
}
