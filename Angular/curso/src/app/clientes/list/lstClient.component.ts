import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../models/cliente';
import { ClienteService } from '../../services/cliente.service';
import { Store } from '@ngrx/store';
import { IStateActionSelect } from 'src/app/reducers/states/IStateActionSelect';
import { Router } from "@angular/router"
import { HandleLocalStorageService } from 'src/app/services/handle-storage.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './lstClient.component.html',
  styleUrls: ['./lstClient.component.css']
})
export class ClientesComponent implements OnInit {
  private listClientes: Cliente[];
  private isActive: boolean = true;
  private paramNgrx: Cliente = new Cliente();
  private lastId: number = 1;
  isTableSaved: boolean;
  keyTable: string = "ngrx";

  constructor(private router: Router, private clienteService: ClienteService, private store: Store<IStateActionSelect>,
    private handleLocalStorageService: HandleLocalStorageService) {
    this.getDataTable();
  }

  ngOnInit() {
    this.handleLocalStorageService.validateLocalStorage(this.keyTable).subscribe(
      (result) => {
        this.isTableSaved = result;
      }
    );

    this.store.select('actionSpecific').subscribe((u => {
      if (u != undefined) {
        if (u.message == "create") {
          this.paramNgrx = new Cliente();
          this.paramNgrx = u.obj;
          this.addNewData();
        }
      }
    }));

  }

  startedLocalStorage() {
    if (this.isTableSaved) {
      this.handleLocalStorageService.getItemLocalStorage(this.keyTable).subscribe(
        (result) => {
          this.listClientes = JSON.parse(result);

        }
      );
    } else {
      this.handleLocalStorageService.saveItemLocalStorage(this.keyTable, this.listClientes);
    }

  }
  public getDataTable() {
    this.clienteService.getClientes().subscribe(
      (cl) => {
        this.listClientes = cl.cliente;
        this.startedLocalStorage();
      }
    );
  }

  addNewData() {
    if (this.paramNgrx.nombre != null) {
      this.paramNgrx.id = this.listClientes.length + 1;
      this.listClientes.push(this.paramNgrx);
      this.handleLocalStorageService.saveItemLocalStorage(this.keyTable, this.listClientes);
    }
  }

  setActivar(): void {
    this.isActive = !this.isActive;
  }
}
