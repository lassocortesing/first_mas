import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../models/cliente';
import { Store } from '@ngrx/store';
import * as CustomActions from 'src/app/reducers/actions/actions'
import { IStateActionSelect } from 'src/app/reducers/states/IStateActionSelect';
import { Router } from '@angular/router';

@Component({
    selector: 'app-frmClientes',
    templateUrl: './frmClient.component.html'
})

export class FrmClientComponent implements OnInit {
    public clienteBinding: Cliente = new Cliente();
    private tittle: string = "Create client";


    constructor(private router: Router, private store: Store<IStateActionSelect>) {
        this.store.select('actionSpecific').subscribe((u: any) => {
            if (u != undefined) {
                if (u.message == "edit") {
                    this.clienteBinding = u.obj;
                }
            }
        });
    }

    ngOnInit() {
    }

    addClient() {
        var obj = this.clienteBinding;
        this.store.dispatch(new CustomActions.GetNew({ message: "create", obj }));
        this.router.navigate(['/clientes']);
    }


}
