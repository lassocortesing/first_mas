import { IState } from 'src/app/models/state-interface';
import * as CustomActions from 'src/app/reducers/actions/actions'

//Funcion de reducer
export function simpleReducer(state: IState, action: CustomActions.Actions) {


  switch (action.type) {
    case CustomActions.SELECT_New:
      return action.payload;

    case CustomActions.SELECT_Edit:
      return action.payload;

    default:
      return state;
  }
}