import { Action } from '@ngrx/store'
import { IState } from 'src/app/models/state-interface'

export const SELECT_New = '[IState] SELECT'
export const SELECT_Edit = '[IState] EDIT'
export const SELECT_Id = '[IState] ID'

export class GetNew implements Action {
    readonly type = SELECT_New
    constructor(public payload: IState) { }
}

export class EditRow implements Action {
    readonly type = SELECT_Edit
    constructor(public payload: IState) { }
}

export type Actions = GetNew | EditRow 