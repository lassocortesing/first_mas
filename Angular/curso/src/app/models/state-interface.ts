import { Cliente } from './cliente';

export interface IState {
    message: string;
    obj: Cliente;
}