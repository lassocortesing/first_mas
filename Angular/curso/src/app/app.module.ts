import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ClientesComponent } from './clientes/list/lstClient.component';
import { FrmClientComponent } from './clientes/form/frmClient.component';

import { ClienteService } from './services/cliente.service';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { simpleReducer } from './reducers/reducer/simpleReducer';


const cntRoutes: Routes = [
  { path: '', redirectTo: '/clientes', pathMatch: 'full' },
  { path: 'clientes', component: ClientesComponent },
  { path: 'clientes/form', component: FrmClientComponent },


];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    FrmClientComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(cntRoutes),
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({ actionSpecific: simpleReducer })
  ],
  providers: [ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
