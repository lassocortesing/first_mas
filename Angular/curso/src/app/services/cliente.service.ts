import { Injectable } from '@angular/core';

import { Cliente } from '../models/cliente';
import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class ClienteService {

  private baseUrl = 'api/clientes.json';
  constructor(private http: HttpClient) { }

  getClientes(): Observable<any> {
    return this.http.get<any>(this.baseUrl);

  }
}
