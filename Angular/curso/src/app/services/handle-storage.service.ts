import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class HandleLocalStorageService {
    constructor() { }

    validateLocalStorage(key: string): Observable<boolean> {
        if (localStorage.getItem(key) != null) {
            return of(true);
        }
        return of(false);
    }

    saveItemLocalStorage(key: string, tabs: any) {
        localStorage.setItem(key, JSON.stringify(tabs));
    }

    getItemLocalStorage(key: string): Observable<string> {
        return of(localStorage.getItem(key));
    }

}